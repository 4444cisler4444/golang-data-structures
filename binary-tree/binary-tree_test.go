package main

import (
	"fmt"
	"strings"
	"testing"
)

func TestTreeAdd(t *testing.T) {
	tree := NewBinaryTree[int]()
	tree.Add(1)

	if !tree.Has(1) {
		t.Fatalf("Failed to find item placed in tree.")
	}

	tree.Add(2)
	tree.Add(3)

	if !tree.Has(2) || !tree.Has(3) {
		t.Fatal("Failed to add item")
	}
}

func TestTreeSer(t *testing.T) {
	tree := NewBinaryTree[int]()
	tree.Add(2)
	tree.Add(1)
	tree.Add(3)
	tree.Add(4)
	tree.Add(5)
	tree.Add(6)

	bytes, err := tree.Ser()
	if err != nil {
		t.Fatalf(err.Error())
	}
	str := string(bytes)
	fmt.Println(str)

	if !strings.Contains(str, "2") {
		t.Fatal("Serialized output does not contain added values")
	}
}


func TestTreeBalancing(t *testing.T) {
	// tree := NewBinaryTree[int]()

	// for i := 0; i < 5; i++ {
	// 	tree.Add(i)
	// }
	// if tree.Depth() > 2 {
	// 	t.Fatal("Balancing failed for small list")
	// }

	// for i := 0; i < 10; i++ {
	// 	tree.Add(i)
	// }
	// if tree.Depth() > 2 {
	// 	t.Fatal("Balancing failed for medium list")
	// }

	// for i := 0; i < 50; i++ {
	// 	tree.Add(i)
	// }
	// if tree.Depth() > 2 {
	// 	t.Fatal("Balancing failed for large list")
	// }
}
