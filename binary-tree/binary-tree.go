package main

import (
	"cmp"
	"encoding/json"
)

type OrdComparable interface {
	cmp.Ordered
	comparable
}

// Self-balancing Binary Search Tree
type TreeNode[T OrdComparable] struct {
	value *T
	left  *TreeNode[T]
	right *TreeNode[T]
}

func NewBinaryTree[T OrdComparable]() TreeNode[T] {
	return TreeNode[T]{nil, nil, nil}
}

func (self *TreeNode[T]) Add(in T) {
	if self.value == nil {
		self.value = &in
		return
	}

	if *self.value < in {
		if self.right != nil {
			self.right.Add(in)
		} else {
			self.right = &TreeNode[T]{&in, nil, nil}
		}
	}

	if *self.value > in {
		if self.left != nil {
			self.left.Add(in)

		} else {
			self.left = &TreeNode[T]{&in, nil, nil}
		}
	}

	self.balance()
}

func (self *TreeNode[T]) balanceFactor() int {
    if self.left == nil || self.right == nil {
        return 0
    }

    balanceFactor := self.left.Depth() - self.right.Depth()
    return balanceFactor
}

func (self *TreeNode[T]) balance() {
    balanceFactor := self.balanceFactor()

    if balanceFactor > 1 {
        if self.left.balanceFactor() < 0 {
            self.left = self.left.rotateLeft()
        }
        self = self.rotateRight()
        return
    }

    if balanceFactor < -1 {
        if self.right.balanceFactor() > 0 {
            self.right = self.right.rotateRight()
        }
        self = self.rotateLeft()
        return
    }
}

func (self *TreeNode[T]) rotateRight() *TreeNode[T] {
	pivot := self.left
	self.left = pivot.right
	pivot.right = self
    return pivot
}

func (self *TreeNode[T]) rotateLeft() *TreeNode[T] {
	pivot := self.right
	self.right = pivot.left
	pivot.left = self
    return pivot
}

func (self *TreeNode[T]) Delete(t T) error {
	// TODO

	self.balance()

	return nil
}

func AsMap[T OrdComparable](tn *TreeNode[T]) map[string]interface{} {
	m := make(map[string]interface{})
	m["value"] = *tn.value

	if tn.left != nil {
		m["left"] = AsMap(tn.left)
	}
	if tn.right != nil {
		m["right"] = AsMap(tn.right)
	}
	return m
}

func (self *TreeNode[T]) Ser() ([]byte, error) {
	ser := AsMap(self)

	return json.Marshal(ser)
}

func (self *TreeNode[T]) Has(t T) bool {
	if *self.value == t {
		return true
	}
	if *self.value < t {
		if self.right != nil {
			return self.right.Has(t)
		}
	}
	if *self.value > t {
		if self.left != nil {
			return self.left.Has(t)
		}
	}
	return false
}

func (self *TreeNode[T]) Depth() int {
	if self.value == nil {
		return 0
	}

	depthL := 0
	if self.left != nil {
		depthL = self.left.Depth()
	}

	depthR := 0
	if self.right != nil {
		depthR = self.right.Depth()
	}

	return 1 + max(depthL, depthR)
}

func (self *TreeNode[T]) Count() int {
	if self.value == nil {
		return 0
	}

	count := 0
	found := []*TreeNode[T]{self}

	for len(found) > 0 {
		lastIdx := len(found) - 1
		node := found[lastIdx]
		found = found[:lastIdx]

		count++

		if node.right != nil {
			found = append(found, node.right)
		}
		if node.left != nil {
			found = append(found, node.left)
		}
	}
	return count
}
