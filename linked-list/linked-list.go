package main

import (
	"encoding/json"
	"errors"
)

// Linked List
type node[T comparable] struct {
	value T
	next  *node[T]
}

func (self *node[T]) Add(in T) {
	self.next = &node[T]{self.value, self.next}
	self.value = in
}

func (self *node[T]) AddToTail(in T) {
	last := self
	for last.next != nil {
		last = last.next
	}
	last.next = &node[T]{value: in}
}

func (self *node[T]) Len() int {
	var out int
	t := self
	for t != nil {
		t = t.next
		out += 1
	}
	return out
}

// func (self *node[T]) ReverseInPlace() {
// 	c := self
// 	for c.next != nil {
// 		c = c.next
// 	}
// 	self.value = c.value
//     // TODO
// }

func (self *node[T]) AsSlice() []T {
	s := []T{}

	tmp := self
	for tmp.next != nil {
		s = append(s, tmp.value)
		tmp = tmp.next
	}
	return s
}

func (self *node[T]) Concat(other *node[T]) {
	temp := self
	for temp.next != nil {
		temp = temp.next
	}
	temp.next = other
}

func (self *node[T]) Reverse() node[T] {
	r := &node[T]{self.value, nil}

	temp := self
	for temp.next != nil {
		temp = temp.next

		r.Add(temp.value)
	}

	return *r
}

func (self *node[T]) Contains(t T) bool {
	tmp := self
	for tmp != nil {
		if tmp.value == t {
			return true
		}
		tmp = tmp.next

	}

	return false
}

func (self node[T]) Ser() ([]byte, error) {
	ser := make(map[string]interface{})
	ser["value"] = self.value

	var nodes []T

	t := self
	for t.next != nil {
		t = *t.next

		nodes = append(nodes, t.value)
	}

	// fold values into next record
	var objs []map[string]interface{}
	for _, n := range nodes {
		o := make(map[string]interface{})
		o["value"] = n
		objs = append(objs, o)
	}

	// attach next from index+1 to each node
	for idx, obj := range objs {
		if idx+1 != len(nodes) {
			obj["next"] = objs[idx+1]
		}
	}

	ser["next"] = objs[0]

	return json.Marshal(ser)
}

func (self *node[T]) Delete(t T) error {
	a := self
	for a.next != nil {
		if a.next.value == t {
			a.next = a.next.next
			return nil
		}

		a = a.next
	}
	return errors.New("value not found")
}

func (self *node[T]) Traverse(f func(t T)) {
	t := self
	f(t.value)
	for t.next != nil {
		t = t.next
		f(t.value)
	}
}

func main() {
	list := node[int]{value: 2}

	list.Add(3)
	list.Add(5)
	list.Add(6)

	list.Delete(5)

	// list.Traverse(func(t int) {
	// 	fmt.Println(t)
	// })
}
