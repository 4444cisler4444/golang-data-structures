package main

import (
	"reflect"
	"strings"
	"testing"
)

func TestAdd(t *testing.T) {
	n := &node[int]{1, nil}
	n.Add(2)

	if n.value != 2 {
		t.Fatalf("Expected Add to add value to front of list")
	}
}

func TestAddToTail(t *testing.T) {
	n := &node[int]{1, nil}
	n.AddToTail(2)

	if n.next.value != 2 {
		t.Fatalf("Expect AddToTail to add value to tail of list")
	}
}

func TestReverse(t *testing.T) {
	n := &node[int]{1, nil}
	n.AddToTail(2)
	n.AddToTail(3)

	rev := n.Reverse()
	if reflect.DeepEqual(n.AsSlice(), rev.AsSlice()) {
		t.Fatalf("Expected reversed to not be the original list")
	}
}

func TestConcat(t *testing.T) {
	n := &node[int]{1, nil}
	n.AddToTail(2)

	n2 := &node[int]{5, nil}
	n2.AddToTail(6)

	n.Concat(n2)

	for _, inNum := range []int{1, 2, 5, 6} {
		if !n.Contains(inNum) {
			t.Fatalf("Expected original list to contain elements from concatenated list")
		}
	}

	for _, outNum := range []int{3, 7, 39} {
		if n.Contains(outNum) {
			t.Fatalf("Original list contains elemnts not from either list")
		}
	}
}

func TestSerialize(t *testing.T) {
	n := &node[int]{1, nil}
	n.AddToTail(2)
	n.AddToTail(3)
	n.AddToTail(4)

	s, err := n.Ser()
	if err != nil {
		t.Fatalf(err.Error())
	}
	ser := string(s)

	size := strings.Count(ser, "value")

	if n.Len() != size {
		t.Fatalf("Serialization error (the sizes do not match)")
	}
}

func TestDelete(t *testing.T) {
  n := &node[int]{1, nil}
  n.Add(2)
  n.Add(3)
  n.Add(4)
  n.Add(5)

  n.Delete(3)

  if n.Len() != 4 {
    t.Fatal("Failed to delete item from list")
  }
}
